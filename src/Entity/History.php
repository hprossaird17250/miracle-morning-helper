<?php

namespace App\Entity;

use App\Repository\HistoryRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: HistoryRepository::class)]
class History
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $user_id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $date = null;

    #[ORM\Column(nullable: true)]
    private ?int $silence = null;

    #[ORM\Column(nullable: true)]
    private ?int $affirmation = null;

    #[ORM\Column(nullable: true)]
    private ?int $visualisation = null;

    #[ORM\Column(nullable: true)]
    private ?int $exercice = null;

    #[ORM\Column(nullable: true)]
    private ?int $reading = null;

    #[ORM\Column(nullable: true)]
    private ?int $writing = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    public function setUserId(int $user_id): static
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): static
    {
        $this->date = $date;

        return $this;
    }

    public function getSilence(): ?int
    {
        return $this->silence;
    }

    public function setSilence(?int $silence): static
    {
        $this->silence = $silence;

        return $this;
    }

    public function getAffirmation(): ?int
    {
        return $this->affirmation;
    }

    public function setAffirmation(?int $affirmation): static
    {
        $this->affirmation = $affirmation;

        return $this;
    }

    public function getVisualisation(): ?int
    {
        return $this->visualisation;
    }

    public function setVisualisation(?int $visualisation): static
    {
        $this->visualisation = $visualisation;

        return $this;
    }

    public function getExercice(): ?int
    {
        return $this->exercice;
    }

    public function setExercice(?int $exercice): static
    {
        $this->exercice = $exercice;

        return $this;
    }

    public function getReading(): ?int
    {
        return $this->reading;
    }

    public function setReading(?int $reading): static
    {
        $this->reading = $reading;

        return $this;
    }

    public function getWriting(): ?int
    {
        return $this->writing;
    }

    public function setWriting(?int $writing): static
    {
        $this->writing = $writing;

        return $this;
    }
}
