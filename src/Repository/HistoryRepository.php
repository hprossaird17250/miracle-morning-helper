<?php

namespace App\Repository;

use App\Entity\History;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<History>
 */
class HistoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, History::class);
    }

     /**
     * @param \DateTimeInterface $date
     * @return History|null
     */
    public function findByDate(\DateTimeInterface $date, int $userId): ?History
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.date = :date')
            ->andWhere('h.user_id = :userId')
            ->setParameter('date', $date)
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
