<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/custom_list')]
class CustomListController extends AbstractController
{

    #[Route('/{user_id}', name: 'custom_list', methods: ['GET'])]
    public function show(): Response
    {
        return $this->json(null);
    }

    #[Route('/', name: 'custom_list_create', methods: ['POST'])]
    public function create(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);

        //TODO: format body

        $entityManager = $this->getDoctrine()->getManager();
       // $entityManager->persist();
        $entityManager->flush();

        return $this->json();
    }

    #[Route('/{id}', name: 'custom_list_delete', methods: ['DELETE'])]
    public function delete(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
       // $entityManager->remove();
        $entityManager->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
