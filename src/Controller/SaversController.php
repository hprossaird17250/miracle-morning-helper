<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SaversController extends AbstractController
{
    #[Route('/savers/{type}', name: 'savers')] //Return savers advices
    public function get(): Response
    {
        $now = new \DateTime();
        return $this->render('base.html.twig', [
            'day' => $now->format('Y-m-d H:i:s')
        ]);
    }
}