<?php
namespace App\Controller;

use App\Entity\History;
use App\Entity\Affirmation;
use App\Repository\HistoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DayController extends AbstractController
{
    #[Route('/day/{date}', name: 'day', methods: ['GET', 'PUT'])]
    public function handleDayTasks(string $date, HistoryRepository $historyRepository, Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        // $user = $this->getUser(); // Assuming you're using Symfony Security for user management
        $userId = 0; // Assuming the User entity has a getId() method

        try {
            $dateTime = new \DateTime($date);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => 'Invalid date format'], 400);
        }

        $currentDate = new \DateTime();
        $currentDate->setTime(0, 0, 0);
        $dateTime->setTime(0, 0, 0);

        $history = $historyRepository->findByDate($dateTime, $userId);

        // Handle GET request
        if ($request->isMethod('GET')) {
            if (!$history) {
                if ($dateTime != $currentDate) {
                    return new JsonResponse(['error' => 'No tasks found for this date'], 404);
                } else {
                    // Create a new History entity for today if not found
                    $history = new History();
                    $history->setUserId($userId);
                    $history->setDate($dateTime);
                    $history->setSilence(0);
                    $history->setAffirmation(0);
                    $history->setVisualisation(0);
                    $history->setExercice(0);
                    $history->setReading(0);
                    $history->setWriting(0);

                    // Persist the new entity to the database
                    $entityManager->persist($history);
                    $entityManager->flush();
                }
            }

            return new JsonResponse([
                'date' => $history->getDate()->format('Y-m-d'),
                'tasks' => [
                    'silence' => $history->getSilence(),
                    'affirmation' => $history->getAffirmation(),
                    'visualisation' => $history->getVisualisation(),
                    'exercice' => $history->getExercice(),
                    'reading' => $history->getReading(),
                    'writing' => $history->getWriting(),
                ]
            ]);
        }

        // Handle PUT request
        if ($request->isMethod('PUT')) {
            if (!$history) {
                return new JsonResponse(['error' => 'No tasks found for this date'], 404);
            }

            $data = json_decode($request->getContent(), true);
            if (json_last_error() !== JSON_ERROR_NONE) {
                return new JsonResponse(['error' => 'Invalid JSON'], 400);
            }

            // Update the history entity with the provided data
            $history->setSilence($data['silence'] ?? $history->getSilence());
            $history->setAffirmation($data['affirmation'] ?? $history->getAffirmation());
            $history->setVisualisation($data['visualisation'] ?? $history->getVisualisation());
            $history->setExercice($data['exercice'] ?? $history->getExercice());
            $history->setReading($data['reading'] ?? $history->getReading());
            $history->setWriting($data['writing'] ?? $history->getWriting());

            // Persist the updated entity to the database
            $entityManager->persist($history);
            $entityManager->flush();

            return new JsonResponse([
                'date' => $history->getDate()->format('Y-m-d'),
                'tasks' => [
                    'silence' => $history->getSilence(),
                    'affirmation' => $history->getAffirmation(),
                    'visualisation' => $history->getVisualisation(),
                    'exercice' => $history->getExercice(),
                    'reading' => $history->getReading(),
                    'writing' => $history->getWriting(),
                ]
            ]);
        }

        return new JsonResponse(['error' => 'Method not allowed'], 405);
    }

    #[Route('/days', name: 'days')]
    public function getDays(): Response
    {
        //Get all days user stat for the user
        $now = new \DateTime();
        return $this->render('base.html.twig', [
            'day' => $now->format('Y-m-d H:i:s')
        ]);
    }

    #[Route('/affirmation', name: 'api_affirmation_random', methods: ['GET'])]
    public function getRandomAffirmation(EntityManagerInterface $entityManager): JsonResponse
    {
        $repository = $entityManager->getRepository(Affirmation::class);

        // Count total number of affirmations
        $count = $repository->count([]);

        if ($count === 0) {
            return new JsonResponse(['message' => 'No affirmations found'], 404);
        }

        // Generate a random index
        $randomIndex = mt_rand(0, $count - 1);

        // Retrieve the random affirmation
        $query = $repository->createQueryBuilder('a')
            ->setFirstResult($randomIndex)
            ->setMaxResults(1)
            ->getQuery();

        $randomAffirmation = $query->getOneOrNullResult();

        if (!$randomAffirmation) {
            return new JsonResponse(['message' => 'No affirmation found'], 404);
        }

        return new JsonResponse([
            'id' => $randomAffirmation->getId(),
            'title' => $randomAffirmation->getTitle(),
            'author' => $randomAffirmation->getAuthor()
        ]);
    }
}