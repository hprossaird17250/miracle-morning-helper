import { audio, selectedSound } from './Silence.vue';

export const playSound = () => {
if (audio) {
audio.pause();
audio = null;
}
if (selectedSound.value) {
audio = new Audio(require(`../sounds/todo.mp3`));
audio.loop = true;
audio.play();
}
};
