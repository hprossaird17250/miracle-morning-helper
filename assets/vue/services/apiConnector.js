const baseURL = 'http://127.0.0.1:8000';

async function getDay(date) {
    const response = await fetch(`${baseURL}/day/${date}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        credentials: 'include',
    });
    if (!response.ok) {
        throw new Error('Failed to fetch day tasks');
    }
    return response.json();
}

async function updateDay(date, tasks) {
    const response = await fetch(`${baseURL}/day/${date}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        credentials: 'include',
        body: JSON.stringify(tasks),
    });
    if (!response.ok) {
        throw new Error('Failed to update day tasks');
    }
    return response.json();
}

async function getAffirmation() {
    const response = await fetch(`${baseURL}/affirmation`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        credentials: 'include',
    });
    if (!response.ok) {
        throw new Error('Failed to fetch affirmmation');
    }
    return response.json();
}
export default {
    getDay,
    updateDay,
    getAffirmation
};
