import { createMemoryHistory, createRouter } from 'vue-router'

import Dashboard from './views/Dashboard.vue'
import Days from './views/Days.vue'
import TodayList from './views/TodayList.vue'
import Silence from './views/Silence.vue'
import Affirmations from './views/Affirmations.vue'
import Visualisation from './views/Visualisation.vue'
import Exercise from './views/Exercise.vue'
import Reading from './views/Reading.vue'
import Scribing from './views/Scribing.vue'

const routes = [
    { path: '/', name: 'dashboard', component: Dashboard },
    { path: '/', name: 'days', component: Days },
    { path: '/', name: 'today', component: TodayList },
    { path: '/', name: 'silence', component: Silence },
    { path: '/', name: 'affirmations', component: Affirmations },
    { path: '/', name: 'visualisation', component: Visualisation },
    { path: '/', name: 'exercise', component: Exercise },
    { path: '/', name: 'reading', component: Reading },
    { path: '/', name: 'scribing', component: Scribing },
]

const router = createRouter({
  history: createMemoryHistory(),
  routes,
})

export default router;